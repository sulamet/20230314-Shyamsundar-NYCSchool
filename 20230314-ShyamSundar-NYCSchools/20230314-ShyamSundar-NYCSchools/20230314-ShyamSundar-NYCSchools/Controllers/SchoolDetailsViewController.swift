//
//  SchoolDetailsViewController.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    
    var viewModel = SchoolDetailsViewModel()
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    private struct Attributes {
        static let contactDetailsCellIdentifier = "ContactDetailsTableViewCell"
        static let scoreTableViewCellIdentifier = "ScoreTableViewCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = StringConstants.SchoolDetails.title
        fetchSchoolDetails()
    }
    
    private func fetchSchoolDetails() {
        activityIndicatorView.startAnimating()
        viewModel.getSchoolDetails(completion: { isSuccess in
            self.activityIndicatorView.stopAnimating()
            if isSuccess {
                self.tableView.reloadData()
            }
        })
    }
}

extension SchoolDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = viewModel.rows[indexPath.row]
        switch row {
        case .schoolInfo:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Attributes.contactDetailsCellIdentifier, for: indexPath) as? ContactDetailsTableViewCell {
                cell.setschoolContactDetails(school: viewModel.selectedSchool!)
                return cell
            }
        case .numberOfSatTestTakers, .criticalReadingScore, .mathScore, .writingScore:
            guard let scoreDetailscell = tableView.dequeueReusableCell(withIdentifier: Attributes.scoreTableViewCellIdentifier, for: indexPath) as? ScoreTableViewCell  else {
                return UITableViewCell()
            }
            scoreDetailscell.setScoreLabel(score: viewModel.getScoreDetailsData(forRow: row))
            return scoreDetailscell
        }
        return UITableViewCell()
    }
}

extension SchoolDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
