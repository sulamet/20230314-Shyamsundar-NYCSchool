//
//  ScoreTableViewCell.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import UIKit

class ScoreTableViewCell: UITableViewCell {
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setScoreLabel(score: String) {
        scoreLabel.text = score
    }
}
