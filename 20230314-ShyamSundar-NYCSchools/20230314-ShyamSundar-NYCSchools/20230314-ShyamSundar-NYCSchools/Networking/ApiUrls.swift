//
//  EndPoints.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 15/03/23.
//

import Foundation

struct ApiUrls {
    
    struct BaseUrl {
        static let cityOfNewyork = "https://data.cityofnewyork.us/resource"
    }
    
    struct EndPoints {
        static let schoolsList = "/s3k6-pzi2.json"
        static let schoolDetails = "/f9bf-2cp4.json?dbn="
    }
}
