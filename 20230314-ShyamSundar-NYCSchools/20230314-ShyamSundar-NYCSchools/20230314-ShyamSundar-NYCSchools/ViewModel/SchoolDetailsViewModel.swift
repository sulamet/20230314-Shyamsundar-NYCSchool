//
//  SchoolDetailsViewModel.swift
//  20230314-ShyamSundar-NYCSchools
//
//  Created by ShyamSundar on 3/15/23.
//

import Foundation

class SchoolDetailsViewModel {
    
    enum SchoolDetailRows: Int {
        case schoolInfo = 0
        case numberOfSatTestTakers
        case criticalReadingScore
        case mathScore
        case writingScore
    }
    
    var selectedSchool : School?
    var schoolDetails: SchoolDetailsModel?
    var rows: [SchoolDetailRows] = [.schoolInfo]
    
    func getSchoolDetails(completion: @escaping (Bool) -> ()) {
        let url = ApiUrls.BaseUrl.cityOfNewyork + ApiUrls.EndPoints.schoolDetails + "\(selectedSchool?.dbn ?? "")"
        NetworkLayer().getSelectedSchoolsDetails(dbn: selectedSchool?.dbn ?? "", endPoint: url, completion: {(schoolDetailsModel, isSuccess) in
            if isSuccess {
                self.schoolDetails = schoolDetailsModel
                self.rows = [.schoolInfo, .numberOfSatTestTakers, .criticalReadingScore, .mathScore, .writingScore]
            }
            completion(isSuccess)
        })
        
    }
    
    func getScoreDetailsData(forRow: SchoolDetailRows) -> String {
        switch forRow {
        case .numberOfSatTestTakers:
            return StringConstants.SchoolDetails.numberOfSatTestTakers + "\(String(describing: schoolDetails?.numOfSatTestTakers ?? StringConstants.SchoolDetails.notAvailable))"
        case .criticalReadingScore:
            return StringConstants.SchoolDetails.satCriticalReadingAvgScore + "\(String(describing: schoolDetails?.satCriticalReadingAvgScore ?? StringConstants.SchoolDetails.notAvailable))"
        case .mathScore:
            return StringConstants.SchoolDetails.satMathAvgScore + "\(String(describing: schoolDetails?.satMathAvgScore ?? StringConstants.SchoolDetails.notAvailable))"
        case .writingScore:
            return StringConstants.SchoolDetails.satWritingAvgScore + "\(String(describing: schoolDetails?.satWritingAvgScore ?? StringConstants.SchoolDetails.notAvailable))"
        default: return StringConstants.SchoolDetails.emptyString
        }
    }
}
